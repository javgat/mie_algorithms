#!/usr/bin/env python3

import numpy as np
from numpy.typing import NDArray
from typing import List, Tuple

PI = np.float128(3.14159265358979323846264338327950288419716939937510)
    
_f64max = np.finfo(np.float64).max
_MAX_COMPLEX = np.complex256(f"{_f64max}+{_f64max}j") * 1.7e+308

_psis_dict: dict = {}
_jis_dict: dict = {}
_ys_dict: dict = {}

def _y_lvecmie(k, n, z):
    if (k, z) in _ys_dict:
        return _ys_dict[(k, z)]
    if k > n:
        ki = k-1
        val = ((2*ki +1)/z)*_y_lvecmie(ki, n, z) - _y_lvecmie(ki-1, n, z)
    elif k == n:
        val = 1
    elif k == n-1:
        val = 0
    else:
        raise Exception("Wrong k or n value for y ricatti")
    if val == np.inf:
        print("Reached infinite value. NMAX too big.")
    _ys_dict[(k, z)] = val
    return val


def _get_A_lvecmie(n: int, m: int, z: np.complex256):
    suma = 0
    for k in range(n, m+1):
        suma += 1/(_y_lvecmie(k, n, z)*_y_lvecmie(k+1, n, z))
    return np.power(suma, -1, dtype=np.complex256)

def _get_m_lvecmie(n, z):
    m = n
    epsilon = 1e-8
    val = 1
    while val > epsilon:
        m += 1
        val = np.abs(np.power((_y_lvecmie(m, n, z) * _y_lvecmie(m+1, n, z)), -1, dtype=np.complex256), dtype=np.float128)
    return m

def _get_dnmax_lvecmie(n, z):
    m = _get_m_lvecmie(n, z)
    return _get_A_lvecmie(n, m, z) - n/z

def _psi(nm1,x):
    if (nm1, x) in _psis_dict:
        return _psis_dict[(nm1, x)]
    if nm1 > 0:
        n = nm1-1
        val = ((2*n +1)/x)*_psi(n,x) - _psi(n-1, x)
    elif nm1 == 0:
        val = np.sin(x, dtype=np.float128)
    elif nm1 == -1:
        val = np.cos(x, dtype=np.float128)
    else:
        raise Exception("Wrong nm1 value for psi")
    if val == np.inf:
        print("Reached infinite value. NMAX too big.")
    _psis_dict[(nm1, x)] = val
    return val


def _ji(nm1,x):
    if (nm1, x) in _jis_dict:
        return _jis_dict[(nm1, x)]
    if nm1 > 0:
        n = nm1-1
        val = ((2*n +1)/x)*_ji(n,x) - _ji(n-1, x)
    elif nm1 == 0:
        val = np.complex256(f"0{np.cos(x, dtype=np.float128):+}j")
    elif nm1 == -1:
        val = np.complex256(f"0{-np.sin(x, dtype=np.float128):+}j")
    else:
        raise Exception("Wrong nm1 value for ji")
    if val == np.inf:
        print("Reached infinite value. NMAX too big.")
    _jis_dict[(nm1, x)] = val
    return val


def _chi(n,z):
    val = _psi(n,z) + _ji(n,z)
    if val == np.inf:
        print("Reached infinite value. NMAX too big.")
    return val


def _dgrande_down(nm1: int, z: np.complex256, nmax: int, nmax_val: np.complex256 = np.complex256(0.0 + 0j)) -> np.ndarray:
    darr = np.zeros(nmax-nm1+1, np.complex256)
    darr[-1] = nmax_val
    for i in range(nmax, nm1, -1):
        parent_pos = i-nm1
        arr_pos = parent_pos-1
        val = i/z - 1/(darr[parent_pos]+i/z)
        darr[arr_pos] = val
    return darr


def _dgrande_up(nm1: int, z: np.complex256, nmax: int) -> np.ndarray:
    # D[n] = (-n/z) + 1/((n/z) - D[n-1])
    darr = np.zeros(nmax-nm1+1, np.complex256)
    darr[0] = np.complex256(1 / np.tan(z))
    for i in range(nm1, nmax):
        ni = i+1
        arr_pos = ni-nm1
        denom = (ni/z) - darr[arr_pos-1]
        if denom == 0:
            # In case it divides by 0, which should be very rare.
            sumand = _MAX_COMPLEX
        else:
            sumand = 1/denom
        val = -ni/z + sumand
        darr[arr_pos] = val
    return darr


def _get_nstop_wiscombe(x: np.float128):
    # Wiscombe criteria
    if x <= 8:
        val =  np.float128(x+4*np.power(x,1/3)+1)
    elif x < 4200:
        val = np.float128(x+4.05*np.power(x,1/3)+2)
    else:
        val = np.float128(x+4*np.power(x,1/3)+2)
    return np.float128(val)


def _get_nstop_epsilon(x: np.float128, epsilon = 1e-08):
    check_val = np.sqrt(1/epsilon)
    abs_val = 1
    i = -1
    while abs_val < check_val:
        i += 1
        val = _ji(i, x)
        abs_val = np.abs(val.imag)
    return np.float128(i)


def _get_an(n, x, m: complex, dg: List[complex]) -> complex:
    prev = (dg[n]/m + n/x)
    nomin: np.complex256 = prev * _psi(n, x) - _psi(n-1, x)
    denom: np.complex256 = prev * _chi(n, x) - _chi(n-1, x)
    if np.inf in (nomin.real, nomin.imag, denom.real, denom.imag):
        print("Maximum reached in get_bn. NMAX too big.")
    return nomin/denom


def _get_bn(n, x, m: complex, dg: List[complex]) -> complex:
    prev = (m*dg[n] + n/x)
    nomin: np.complex256 = prev * _psi(n, x) - _psi(n-1, x)
    denom: np.complex256 = prev * _chi(n, x) - _chi(n-1, x)
    if np.inf in (nomin.real, nomin.imag, denom.real, denom.imag):
        print("Maximum reached in get_bn. NMAX too big.")
    return nomin/denom


def _get_qs(x, nstop, m, dg):
    s = np.float128(0.0)
    for i in range(1, nstop+1):
        s += (2*i + 1)*(np.abs(_get_an(i, x, m, dg))**2 + np.abs(_get_bn(i, x, m, dg))**2)
    return 2*s/(x**2)


def _get_qe(x: np.float128, nstop: int, m: np.complex256, dg):
    s = np.float128(0.0)
    for i in range(1, nstop+1):
        s += (2*i + 1)*(_get_an(i, x, m, dg)+ _get_bn(i, x, m, dg)).real
    return 2*s/(x**2)


def _get_qback(x: np.float128, nstop: int, m, dg):
    suma = 0
    for n in range(1, nstop+1):
        suma += (2*n - 1)*np.power(-1, n)*(_get_an(n, x, m, dg)+_get_bn(n, x, m, dg))
    absol = np.abs(suma, dtype=np.float128)
    val = (2/np.square(x, dtype=np.float128)) * np.square(absol, dtype=np.float128)
    return val


def print_data(x, m, nstop, nmax, dg):
    print(f"D from 0 to {nmax}")
    print(dg)
    candelabros = np.array([_psi(i, x) for i in range(-1, nstop+1)])
    xis = np.array([_ji(i, x) for i in range(-1, nstop+1)])
    ans = np.array([_get_an(i, x, m, dg) for i in range(1, nstop+1)])
    bns = np.array([_get_bn(i, x, m, dg) for i in range(1, nstop+1)])
    print(f"Candelabros: {candelabros}")
    print(f"Xis: {xis}")
    print(f"ans: {ans}")
    print(f"bns: {bns}")


class Mie:

    _DEFAULT_EPSILON = np.float128(1e-08)
    _EXTRA_NMAX = 35

    def __init__(self, x: np.float128, m: np.complex256):
        self.x = x
        self.m = m
        self.z = m*x
        self._nstop: int = None
        self._nmax: int = None
        self._d: NDArray[np.complex256] = None
        self._qe: np.float128 = None
        self._qs: np.float128 = None
        self._qback: np.float128 = None
        self._lvecmie_m: np.complex256 = None
    
    def set_xm(self, x: np.float128, m: np.complex256):
        self.x = x
        self.m = m
        self.z = m*x
        self._nstop = None
        self._nmax = None
        self._d = None
        self._qe = None
        self._qs = None

    def get_nstop(self) -> int:
        if self._nstop is None:
            self.set_nstop_epsilon()
        return self._nstop
    
    def get_nmax(self) -> int:
        return self.get_nstop() + Mie._EXTRA_NMAX

    def get_d(self) -> NDArray[np.complex256]:
        if self._d is None:
            self.calc_d_down()
        return self._d

    def get_qe(self) -> np.float128:
        if self._qe is None:
            self._qe = _get_qe(self.x, self.get_nstop(), self.m, self.get_d())
        return self._qe

    def get_qs(self) -> np.float128:
        if self._qs is None:
            self._qs = _get_qs(self.x, self.get_nstop(), self.m, self.get_d())
        return self._qs
    
    def get_qa(self) -> np.float128:
        return self.get_qe() - self.get_qs()
    
    def get_qback(self) -> np.float128:
        if self._qback is None:
            self._qback = _get_qback(self.x, self.get_nstop(), self.m, self.get_d())
        return self._qback
    
    def set_nstop_wiscombe(self):
        self._d = None
        self._nstop = int(np.ceil(_get_nstop_wiscombe(self.x)))

    def set_nstop_epsilon(self, epsilon: np.float128 = _DEFAULT_EPSILON):
        self._d = None
        self._nstop = int(np.ceil(_get_nstop_epsilon(self.x, epsilon)))

    def calc_d_up(self):
        self._d = _dgrande_up(0, self.z, self.get_nmax())

    def calc_d_down(self, nmax_val: np.complex256 = np.complex256(0.0 + 0j)):
        self._d = _dgrande_down(0, self.z, self.get_nmax(), nmax_val)
    
    def calc_d_down_lvecmie(self):
        self._lvecmie_m = _get_dnmax_lvecmie(self.get_nmax(), self.z)
        self.calc_d_down(self._lvecmie_m)



def check_test_values():
    ms = [0.75+0.0j, 0.75+0.0j, 0.75+0.0j, 0.75+0.0j, 1.33-1e-5j, 1.33-1e-5j, 1.5-1j, 1.5-1j, 1.5-1j, 1.5-1j, 10-10j, 10-10j, 10-10j]
    xs = [0.099, 0.101, 10, 1000, 100, 10000, 0.055, 0.056, 100, 1000, 1, 100, 10000]
    qes = [7.41786e-5, 8.03354e-6, 2.23226, 1.99791, 2.10132, 2.00409, 0.101491, 0.103347, 2.09750, 2.00437, 2.53229, 2.07112, 2.00591]
    # Possible mmisprints: 7.41786e-5, 2.53229.
    qss = [7.41786e-5, 8.03354e-6, 2.23226, 1.99791, 2.09659, 1.72386, 1.13169e-5, 1.21631e-5, 1.28370, 1.23657, 2.04941, 1.83679, 1.79539]
    mie = Mie(0, 0)
    for x, m, qe_exp, qs_exp in zip(xs, ms, qes, qss):
        mie.set_xm(x, m)
        mie.set_nstop_epsilon()
        mie.calc_d_down_lvecmie()
        #mie.calc_d_up()
        qe = mie.get_qe()
        qs = mie.get_qs()
        print(f"x: {x}, m: {m}")
        print(f"SIM\tQE: {qe:.6g}\tQS: {qs:.6g}")
        print(f"EXP\tQE: {qe_exp}\tQS: {qs_exp}")
        print()


def main():
    # 0.75+0j, 10, 3.10543
    #lamb = np.float128(1)
    #r = np.float128(2)
    #print(f"Lambda: {lamb}\nr: {r}")
    #m = np.complex256(1.55+0j)
    #x = np.float128(5.213)
    check_test_values()
    return


if __name__ == "__main__":
    main()
