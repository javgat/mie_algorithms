from typing import Tuple, List

import numpy as np

import mie

_pifs: dict = {}

def pif(n: int, theta: np.float128) -> np.float128:
    if (n, theta) in _pifs:
        return _pifs[(n, theta)]
    if n == 0:
        val = 0
    elif n == 1:
        val =  1
    else:
        restando  = (n/(n-1)) * pif(n-2, theta)
        val = ((2*n-1)/(n-1)) * np.cos(theta, dtype=np.float128) * pif(n-1, theta) - restando
    val = np.float128(val)
    _pifs[(n, theta)] = val
    return val

def tau(n: int, theta: np.float128) -> np.float128:
    if n < 1:
        raise Exception("Tau function can't be called for n < 1")
    restando = (n+1) * pif(n-1, theta)
    val = n * np.cos(theta, dtype=np.float128) * pif(n, theta) - restando
    return val

def get_splus(theta: np.float128, nstop: int, x: np.float128, m: np.complex256, dg: List[np.complex256]):
    # s1 + s2
    suma = 0
    for n in range(1, nstop+1):
        val = ((2*n + 1)/(n*(n+1)))
        val = val * (mie._get_an(n, x, m, dg) + mie._get_bn(n, x, m, dg))
        val = val * (pif(n, theta) + tau(n, theta))
        suma += val
    return suma

def get_sminus(theta: np.float128, nstop: int, x: np.float128, m: np.complex256, dg: List[np.complex256]):
    # s1 - s2
    suma = 0
    for n in range(1, nstop+1):
        val = ((2*n + 1)/(n*(n+1)))
        val = val * (mie._get_an(n, x, m, dg) - mie._get_bn(n, x, m, dg))
        val = val * (pif(n, theta) - tau(n, theta))
        suma += val
    return suma

def get_s1s2(theta: np.float128, nstop: int, x: np.float128, m: np.complex256, dg: List[np.complex256]) -> Tuple[np.complex256, np.complex256]:
    splus = get_splus(theta, nstop, x, m, dg)
    sminus = get_sminus(theta, nstop, x, m, dg)
    s1 = (splus+sminus)/2
    s2 = splus-s1
    return s1, s2


def fase(theta: np.float128, nstop: int, x: np.float128, m: np.complex256, dg: List[np.complex256], qs: np.float128):
    s1, s2 = get_s1s2(theta, nstop, x, m, dg)
    val = 2*(s1*s1.conjugate() +s2*s2.conjugate())/(np.square(x)*qs)
    return val.real


def get_qe_check(x, m, nstop, dg):
    s1, s2 = get_s1s2(0, nstop, x, m, dg)
    qe = (4/np.square(x))*s1.real
    return qe

def get_qback_check(x, m, nstop, dg):
    s1, s2 = get_s1s2(mie.PI, nstop, x, m, dg)
    qe = (4/np.square(x))*abs(s1)*abs(s1)
    return qe


def main():
    vals = []
    x = 1
    m = np.complex256(complex(1.5, -0.1))
    _mie = mie.Mie(x, m)
    _mie.set_nstop_epsilon()
    qe = _mie.get_qe()
    qs = _mie.get_qs()
    qa = _mie.get_qa()
    qback = _mie.get_qback()
    nstop = _mie.get_nstop()
    dg = _mie.get_d()
    integration = 0
    for theta in range(181):
        thetapr = np.deg2rad(theta)
        val = fase(thetapr, nstop, x, m, dg, qs)
        integration += val * np.sin(thetapr, dtype=np.float128)
        vals.append(val)
    print(0.5 * np.deg2rad(integration))
    print(qe, qs, qa)
    print(f"SSA: {qs/qa}")
    qe2 = get_qe_check(x, m, nstop, dg)
    qback2 = get_qback_check(x, m, nstop, dg)
    print(qe, qback)
    print(qe2, qback2)

if __name__ == "__main__":
    main()
